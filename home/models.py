from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.
gender_choice = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other'),
)

class Customer(models.Model):
    first_name = models.CharField(max_length=200, null=True, blank=True)
    last_name = models.CharField(max_length=200, null=True, blank=True)
    email = models.CharField(max_length=200, null=True, blank=True)
    phone = models.CharField(max_length=200, null=True, blank=True)
    gender = models.CharField(max_length=10, choices=gender_choice, null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.first_name

class CustomerAccount(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE)
    account_number = models.IntegerField(unique=True, max_length=20, null=False)
    account_type = models.CharField(max_length=30, blank=True, null=True)
    balance = models.FloatField(blank=True)

    def __str__(self):
        return str(self.account_number)

class AccountTransaction(models.Model):
    account = models.ForeignKey(User, on_delete=models.CASCADE)
    receiver_account = models.IntegerField(null=False)
    amount = models.FloatField(blank=False, null=False)
    message = models.CharField(max_length=250, null=True)
    created_at = models.DateTimeField(default=datetime.now, null=True, blank=True)

    def __str__(self):
        return str(f"{self.receiver_account} - {self.amount} - {self.message}")

class Loan(models.Model):
    account = models.OneToOneField(User, on_delete=models.CASCADE)
    amount = models.FloatField(blank=False, null=False)
    message = models.TextField(max_length=1000, null=True)
    # status = models.CharField(max_length=100, null=True)
    created_at = models.DateTimeField(default=datetime.now, null=True, blank=True)

    def __str__(self):
        return str(self.account)