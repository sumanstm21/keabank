from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name="home"),
    path('account/', views.account, name="account"),
    path('transferamount/', views.transferamount, name="transferamount"),
    path('accounthistory/', views.accounthistory, name="accounthistory"),
    path('loan/', views.loan, name="loan")
]
