from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from loginmanager.decorators import unauthenticated_user, admin_only
from .models import CustomerAccount, Loan, AccountTransaction
from .forms import LoanForm, AmountTransferForm

# Create your views here.
@login_required(login_url='loginmanager-login')
def home(request):
    return render(request, 'home/index.html')

def account(request):
    data = CustomerAccount.objects.filter(customer=request.user.id)
    context = {'data': data}
    return render(request, 'home/account.html', context)

def transferamount(request):
    form = AmountTransferForm()
    if request.method == "POST":
        form = AmountTransferForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'home/transferamount.html')

    context = {'form': form}
    return render(request, 'home/transferamount.html', context)

def accounthistory(request):
    data = AccountTransaction.objects.filter(account=request.user.id)
    context = {'data': data}
    return render(request, 'home/accounthistory.html', context)

@admin_only
def loan(request):
    Loanfound = None
    try:
        Loanfound = request.user.loan.account
    except:
        pass

    form = LoanForm()
    if request.method == "POST":
        form = LoanForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'home/loan.html')

    context = {'form': form, 'loan': Loanfound}
    return render(request, 'home/loan.html', context)