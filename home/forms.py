from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from .models import *

class LoanForm(ModelForm):
    class Meta:
        model = Loan
        fields = '__all__'
        # exclude = ['user', 'rate_per_hour']
        # widgets = {'user': forms.HiddenInput()}

class AmountTransferForm(ModelForm):
    class Meta:
        model = AccountTransaction
        fields = '__all__'
