from django.contrib import admin
from .models import Customer, CustomerAccount, AccountTransaction, Loan

# Register your models here.
admin.site.register(Customer)
admin.site.register(CustomerAccount)
admin.site.register(AccountTransaction)
admin.site.register(Loan)
