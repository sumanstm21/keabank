# Django - Mandatory 1 - Keabank

By Suman, Ilva and Sari


## ER Diagram

[See our ERD here](https://drive.google.com/file/d/1FodwhBuBiOcAQ3mcdbiKdFangIyqj9HA/view?usp=sharing)


## Repository

[https://gitlab.com/sumanstm21/keabank](https://gitlab.com/sumanstm21/keabank)


## Introduction Text

We included an ER Diagram that we used for the planning of the database tables and the cardinality between the tables. The database includes four tables – customer, bank account, history log and loan. We gave the table columns Primary and Foreign keys to show the cardinality and relations between. 

Later on in the process we agreed on changing the table names in order to make it more clear what each table is for. For instance we changed the table name from historyLog to accountTransaction. In addition we added needed fields to some of the tables or removed fields we did not make use of such as the password field in the bank account (now customer account) table as we decided due to time issues to implement a login as described down below instead of a login with the bank account number and password like originally planned.

Customer table is an input form that is created by an admin, Customer account is also created by admin, with a timestamp, which we are also using in the account transaction. Rest of the columns are input fields on the user side. Same for the loan table, that can be used by a specific group of bank accounts.

The project includes 2 apps – loginmanager and home. Log In requires username and a password, then it navigates to index.html.

The “employee” access is an admin, that can also be edited by adding more admin users with right permits to edit and add customer table contents. The admin has form inputs for tables.

## Login to banking app

Go to: `localhost:8000`

username: testuser <br />
password: test_1234

## Login as a bank employee / admin

Go to: `localhost:8000/admin`

username: admin <br />
password: admin

